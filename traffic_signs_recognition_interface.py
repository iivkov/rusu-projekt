﻿import tkinter as tk
from tkinter import filedialog
from tkinter import *
from PIL import ImageTk, Image
import numpy
from keras.models import load_model

#Load the trained model to classify sign
model = load_model('traffic_signs_recognition_model.h5')

#Open the CSV file to label all traffic signs class
labelNames = open("traffic_signs_classes.csv").read().strip().split("\n")[1:]
labelNames = [l.split(",")[1] for l in labelNames]

def classify(file_path):
  image = Image.open(file_path)
  image = image.resize((30, 30))
  image = numpy.expand_dims(image, axis=0)
  image = numpy.array(image)
  pred = model.predict_classes([image])[0]
  sign = labelNames[pred]
  print(sign)
  result.configure(text=sign)

def show_classify_btn(file_path):
  classify_b=Button(top, text="Prepoznaj znak", command=lambda: classify(file_path), padx=10, pady=5)
  classify_b.configure(bg='#364156', fg='white', font=('arial', 10, 'bold'))
  classify_b.place(relx=0.79, rely=0.46)

def upload_image():
  try:
    file_path=filedialog.askopenfilename()
    uploaded=Image.open(file_path)
    uploaded.thumbnail(((top.winfo_width()/2.25), (top.winfo_height()/2.25)))
    im=ImageTk.PhotoImage(uploaded)
    sign_image.configure(image=im)
    sign_image.image=im
    result.configure(text='')
    show_classify_btn(file_path)
  except:
    pass

if __name__=="__main__":
  #initialise GUI
  top=tk.Tk()
  top.geometry('800x600')
  top.title('Prepoznavanje prometnih znakova')
  top.configure(bg='#f9f6f7')
  heading = Label(top, text="Prepoznavanje prometnih znakova", pady=20, font=('arial', 20, 'bold'))
  heading.configure(background='#f9f6f7', fg='#364156')
  heading.pack()
  result=Label(top, font=('arial', 15, 'bold'))
  result.configure(fg='#011638', bg='#f9f6f7')
  sign_image = Label(top)
  upload=Button(top, text="Učitaj sliku", command=upload_image, padx=10, pady=5)
  upload.configure(background='#364156', fg='white', font=('arial', 10, 'bold'))
  upload.pack(side=BOTTOM, pady=50)
  sign_image.pack(side=BOTTOM, expand=True)
  result.pack(side=BOTTOM, expand=True)
  top.mainloop()