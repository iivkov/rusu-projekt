import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPool2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout

#Variables
data = []
labels = []
classes = 43
current_directory = os.getcwd()

#The dataset has 43 classes
for index in range(classes):
  path = os.path.join(current_directory, 'train', str(index))
  images = os.listdir(path)
#Iterating on all the images of the index folder
  for img in images:
    try:
      image = Image.open(path + '\\'+ img)
      image = image.resize((30, 30))
      image = np.array(image)
      data.append(image)
      labels.append(index)
    except:
        print("Error!")
data = np.array(data)
labels = np.array(labels)
print(data.shape, labels.shape)

#Splitting the dataset
x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, random_state=42)
print("Shape of x_train: ", x_train.shape, " and y_train:", y_train.shape)
print("Shape of x_test: ", x_test.shape, " and y_test:", y_test.shape)
#One hot encoding the labels
y_train = to_categorical(y_train, 43)
y_test = to_categorical(y_test, 43)

#Building the model architecture
model = Sequential()
model.add(Conv2D(filters=32, kernel_size=(5, 5), activation='relu', input_shape=x_train.shape[1:]))
model.add(Conv2D(filters=32, kernel_size=(5, 5), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(rate=0.25))
model.add(Conv2D(filters=64, kernel_size=(3, 3), activation='relu'))
model.add(Conv2D(filters=64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(rate=0.25))
model.add(Flatten())
model.add(Dense(256, activation='relu'))
model.add(Dropout(rate=0.5))
model.add(Dense(43, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

#Training and saving the model
history = model.fit(x_train, y_train, batch_size=64, epochs=15, validation_data=(x_test, y_test))
model.save('traffic_signs_recognition_model.h5')

#Plot the graphs for accuracy and loss
plt.figure(0)
plt.plot(history.history['accuracy'], label='training accuracy')
plt.plot(history.history['val_accuracy'], label='validation accuracy')
plt.title('Accuracy')
plt.xlabel('epochs')
plt.ylabel('accuracy')
plt.legend()
plt.figure(1)
plt.plot(history.history['loss'], label='training loss')
plt.plot(history.history['val_loss'], label='validation loss')
plt.title('Loss')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.legend()

#Testing the model
y_test = pd.read_csv('Test.csv')
test_labels = y_test["ClassId"].values
img_paths = y_test["Path"].values
test_data=[]
for path in img_paths:
  image = Image.open(path)
  image = image.resize((30,30))
  test_data.append(np.array(image))
test_data = np.array(test_data)
prediction = model.predict_classes(test_data)
#Accuracy with the test data
accuracy_score(test_labels, prediction)